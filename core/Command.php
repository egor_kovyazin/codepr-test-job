<?php

namespace core;

/**
 * Базовый класс для консольных команд.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 * @example commands/ParseCommand.php
 */
class Command
{
	/**
	 * Флаг отображения логов в консоли.
	 * @var boolean
	 */
	protected $_isVerbose;

	/**
	 * Конструктор.
	 * @param boolean $isVerbose отображать ли логи в консоли.
	 */
	public function __construct($isVerbose = false)
	{
		$this->_isVerbose = $this->_isVerbose ?: $isVerbose;
	}

	/**
	 * Вывод сообщений в консоль при работе команды.
	 * @param string $message сообщение к отображению.
	 */
	public function log($message)
	{
		if ($this->_isVerbose) {
			echo gmdate('Y-m-d H:i:s') . ': ' . $message . PHP_EOL;
		}
	}
}
