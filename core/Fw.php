<?php

namespace core;

use Exception;

/**
 * Базовый класс фреймворка.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 */
class Fw
{
	const LOG_LEVEL_INFO  = 'INFO';
	const LOG_LEVEL_ERROR = 'ERROR';

	/** @var string */
	private static $_basePath;

	/** @var \core\Application */
	private static $_app;

	/**
	 * Создание приложения.
	 * @param string $className класс прилоежния.
	 * @param array $config настройки.
	 * @return \core\Application
	 * @throws Exception если приложение уже создано.
	 */
	protected static function createApplication($className, $config = null)
	{
		if (isset(self::$_app)) {
			throw new Exception('The application is already initialized.');
		}

		self::$_basePath = isset($config['base_path']) ? $config['base_path']: __DIR__ . '/..';
		self::$_app = new $className($config);

		return self::$_app;
	}

	/**
	 * Создание web-прилоежения.
	 * @param array $config настройки.
	 * @return \core\WebApplication
	 */
	public static function createWebApplication($config = null)
	{
		return self::createApplication('\core\WebApplication', $config);
	}

	/**
	 * Создание консольного приложения.
	 * @param array $config настройки.
	 * @return \core\ConsoleApplication
	 */
	public static function createConsoleApplication($config = null)
	{
		return self::createApplication('\core\ConsoleApplication', $config);
	}

	/**
	 * Ссылка на текущее прилоежние.
	 * @return Application
	 */
	public static function app()
	{
		return self::$_app;
	}

	/**
	 * Путь до конрня проекта.
	 * @return string
	 */
	public static function getBasePath()
	{
		return self::$_basePath;
	}

	/**
	 * Логирование в файл.
	 * @param string $message
	 * @param string $level
	 * @param string $category
	 */
	public static function log($message, $level = self::LOG_LEVEL_INFO, $category = 'application')
	{
		$fileName = self::getBasePath() . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $category . '.log';
		$text = gmdate('Y-m-d H:i:s') . '::' . $level . '::' . $message . PHP_EOL;

		$file = @fopen($fileName,'a');
		@flock($file, LOCK_EX);
		@fwrite($file,$text);
		@flock($file,LOCK_UN);
		@fclose($file);
	}
}
