<?php

namespace core;

use ReflectionClass;
use ReflectionMethod;
use Exception;

/**
 * Консольное прилоежние.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 * @uses fwc
 */
class ConsoleApplication extends Application
{
	/**
	 * Action по умолчанию во всех командах.
	 * @var string
	 */
	public $defaultCommandAction = 'index';

	/**
	 * Размещение команд.
	 * @var string
	 */
	public $commandNamespace = 'commands';

	/**
	 * Запуск команды.
	 * @param array $route as [
	 *   'command' => <command name>,
	 *   'action'  => <action name>,
	 *   'args'    => <list of arguments for action>
	 *   'verbose' => <flag: display log>,
	 * ]
	 */
	public function runCommand($route)
	{
		$commandId = $route['command'];
		$className = $this->commandNamespace . '\\' . ucfirst($commandId) . 'Command';

		$command  = new $className($route['verbose']);
		$actionId = 'action' . ucfirst(($route['action'] ?: $this->defaultCommandAction));

		$method = new ReflectionMethod($command, $actionId);
		$method->invokeArgs($command, $route['args']);
	}

	/**
	 * Старт приложения.
	 */
	public function run()
	{
		parent::run();
		$this->runCommand($this->parseInput());
	}

	/**
	 * Парсинг введенной команды в консоли.
	 * @return array as [
	 *   'command' => <command name>,
	 *   'action'  => <action name>,
	 *   'args'    => <list of arguments for action>
	 *   'verbose' => <flag: display log>,
	 * ]
	 * @throws Exception если команда некорректна.
	 */
	protected function parseInput()
	{
		if (empty($_SERVER['argv'])) {
			throw new Exception('Input string incorrect.');
		}

		$args = $_SERVER['argv'];
		array_shift($args);

		if (in_array('--help', $args) || empty($args)) {
			$this->showHelp();
			$this->end();
		}

		$command = !empty($args[0]) ? strtolower($args[0]): null;
		array_shift($args);

		$action = 'index';
		if (!empty($args[0]) && strpos($args[0], '--') === false) {
			$action =  $args[0];
			array_shift($args);
		}

		$actionArgs = [];
		$verbose    = false;

		foreach ($args as $arg) {
			if ($arg == '--verbose') {
				$verbose = true;
				continue;
			}

			$param = explode('=', trim($arg, '-'));
			$actionArgs[$param[0]] = $param[1];
		}

		return [
			'command' => $command,
			'action'  => $action,
			'args'    => $actionArgs,
			'verbose' => $verbose,
		];
	}

	/**
	 * Отображение неперехваченных исключений.
	 * @param Exception $exception
	 */
	public function exceptionHandler(Exception $exception)
	{
		restore_error_handler();
		restore_exception_handler();

		echo PHP_EOL . str_pad('', 20, '=') . PHP_EOL . PHP_EOL;
		echo 'Error:' . PHP_EOL;
		echo '....' . $exception->getMessage() . PHP_EOL;
		echo PHP_EOL . PHP_EOL . str_pad('', 20, '=') . PHP_EOL;

		$this->end(1);
	}

	/**
	 * Отображение справки и списка доступных команд.
	 */
	public function showHelp()
	{
		echo PHP_EOL . str_pad('', 20, '=') . PHP_EOL . PHP_EOL;
		echo 'Available commands:' . PHP_EOL;

		$commands = $this->findCommands();

		foreach ($commands as $commandName => $actions) {
			foreach ($actions as $actionName => $params) {

				$cliParams = array_map(function($param, $value) {
					return "--$param=" . ($value ?: 'value');
				}, array_keys($params), $params);

				echo "  - $commandName $actionName " 
					. join(' ', $cliParams) 
					. ' [--verbose]' . PHP_EOL;
			}

			echo PHP_EOL;
		}

		echo PHP_EOL;
	}

	/**
	 * Полный список команд.
	 * @return array as [
	 *   <command name> => [
	 *     <action name> => <params as array>,
	 *     ...
	 *   ],
	 *   ...
	 * ]
	 * 
	 * <pre>
	 * Available commands:
	 *   - parse  --csvfile=value [--verbose]
	 * 
	 *   - createtables   [--verbose]
	 * </pre>
	 */
	protected function findCommands()
	{
		$commands = [];
		$classes = $this->findCommandsClasses();

		foreach ($classes as $commandName => $path) {
			$class = new ReflectionClass($path);

			foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
				$action = $method->getName();
				if (strncasecmp($action, 'action', 6) || strlen($action) <= 6) {
					continue;
				}

				$action = strtolower(substr($action, 6));

				$commands[$commandName][($action == 'index' ? '': $action)] = $this->getActionParams($method);
			}
		}

		return $commands;;
	}

	/**
	 * Список параметров для метода с отображением значений по умолчанию.
	 * @param ReflectionMethod $method
	 * @return array as [
	 *   <param name> => <defaul value|null>,
	 *   ...
	 * ]
	 */
	protected function getActionParams(ReflectionMethod $method)
	{
		$params = [];
		foreach ($method->getParameters() as $param) {
			$optional     = $param->isDefaultValueAvailable();
			$defaultValue = $optional ? $param->getDefaultValue() : null;

			if (is_array($defaultValue)) {
				$defaultValue = str_replace(array("\r\n", "\n", "\r"), "", print_r($defaultValue, true));
			}

			$name = $param->getName();
			if ($name === 'args') continue;

			$params[$name] = ($optional) ? $defaultValue: null;
		}

		return $params;
	}

	/**
	 * Список классов команд.
	 * @return array as [
	 *   <command name> => <path to class>,
	 *   ...
	 * ]
	 */
	protected function findCommandsClasses() {
		$path = Fw::getBasePath() . DIRECTORY_SEPARATOR . $this->commandNamespace;

		if (($dir = @opendir($path)) === false) {
			return [];
		}

		$commands = [];
		while (($name = readdir($dir)) !== false) {
			$file = $path . DIRECTORY_SEPARATOR . $name;

			if (!strcasecmp(substr($name, -11), 'Command.php') && is_file($file)) {
				$commands[strtolower(substr($name, 0, -11))] = 
					'\\' . $this->commandNamespace . '\\' . str_replace('.php', '', $name);
			}
		}
		closedir($dir);
		return $commands;
	}
}
