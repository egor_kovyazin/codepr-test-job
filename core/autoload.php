<?php
/**
 * Инициазация загрузчика для автозагрузки файлов.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 */
require_once __DIR__ . '/ClassLoader.php';

spl_autoload_register(['ClassLoader', 'loadClass']);
