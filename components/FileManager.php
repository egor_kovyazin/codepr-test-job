<?php

namespace components;

use core\Fw;
use Exception;

/**
 * Класс для поиска и отображения файлов в локальной директории.
 * 
 * 1) Для поиска используется функция glob
 *   На линуксе можно использовать "find <DIR> -maxdepth 1 -type f -name <SEACRH NAME>" (так будет гибче)
 * 
 * 2) Контант файла возвращается в виде массива строк.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class FileManager
{
	/**
	 * Директория, в которой вести поиск/просмотр.
	 * @var string
	 */
	private $_dir;

	/** @var \components\FileManager */
	private static $_instance;

	/**
	 * Инициализация объекта.
	 * @return \components\FileManager
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/*
	 * Смотрим настройки в конфиге в секции $config['params']['fileManager']
	 */
	public function __construct()
	{
		$params = Fw::app()->getParams();
		if (isset($params['fileManager'])) {
			$this->_dir = isset($params['fileManager']['dir'])
				? $params['fileManager']['dir']
				: __DIR__;
		}
	}

	/**
	 * Поиск по шаблону.
	 * @param string $pattern
	 * @return array список файлов вида
	 * [
	 *   [
	 *     'name' => <string: file name>,
	 *     'size' => <string: size>
	 *   ],
	 *   ...
	 * ]
	 */
	public function search($pattern = null)
	{
		$pattern = $pattern ?: '*';

		if ($this->checkForbiddenCharacters($pattern)) {
			return [];
		}

		$files = glob($this->_dir . DIRECTORY_SEPARATOR . $pattern);

		foreach ($files as $index => $file) {
			if (is_dir($file)) {
				unset($files[$index]);
				continue;
			}

			$files[$index] = [
				'name' => str_replace($this->_dir . DIRECTORY_SEPARATOR, '', $file),
				'size' => filesize($file),
			];
		}

		return $files ?: [];
	}

	/**
	 * Возвращает содержимое файла в виде массива строк.
	 * 
	 * @todo учитывая, что размер файла может быть большой, 
	 * можно применить генератор, чтобы не пихать массив в память.
	 * 
	 * @param string $fileName имя файла относительно текущей директории.
	 * @return mixed array|null строки файла или null, если файла нет.
	 */
	public function getContent($fileName)
	{
		if (!$this->checkFileName($fileName)) {
			return null;
		}

		$fileRows = [];
		$handle = @fopen($this->_dir . DIRECTORY_SEPARATOR . $fileName, 'r');
		if ($handle) {
			while (!feof($handle)) {
				$fileRows[] = fgets($handle, 4096);
			}
			fclose($handle);
		}

		return $fileRows;
	}

	/**
	 * Проверка, что файл лежит в нужной директории, и он существует.
	 * @param string $fileName имя файла относительно текущей директории.
	 * @return boolean TRUE если ошибок нет.
	 */
	private function checkFileName($fileName)
	{
		$fullName = $this->_dir . DIRECTORY_SEPARATOR . $fileName;

		if (dirname($fullName) !== $this->_dir) {
			return false;
		}

		if (!file_exists($fullName)) {
			return false;
		}

		return true;
	}

	/**
	 * Проверкпа поисковой строки на содержание запрещенных символов.
	 * @param string $pattern
	 * @return boolean TRUE если строка содержит запрещенные символы.
	 */
	private function checkForbiddenCharacters($pattern)
	{
		$blackCharacters = ['/'];

		foreach ($blackCharacters as $character) {
			if (false !== strpos($pattern, $character)) {
				return true;
			}
		}

		false;
	}
}
