Тестовое задание для кандита в PHP-программисты codepr.ru
============================

СТРУКТУРА ПРОЕКТА
------------------

      commands/               консольные команды
      components/             компоненты
      config/                 конфиги
      controllers/            контроллеры
      core/                   ядро мини-фреймворка
      
          Application        базовый класс прилоежния
          ClassLoader        загрузчик классов
          Command            базовый класс команды
          ConsoleApplication консольное приложение
          Controller         базовый класс контроллера
          Fw                 базовый класс фреймворка (аналог Yii)
          WebApplication     веб-приложение
      
      models/                модели
      runtime/               логи приложения
      vagrant/               настройки VAGRANT
      views/                 вьюшки
      web/                   публичные скрипты
      
      fwc                    консольные команды


ЗАВИСИМОСТИ
------------

Полный список зависимостей можно посмотреть по рецептам Chef в vagrant/cookbooks/recipes.
Кратко список такой:

    * nginx
    * PHP v5.5.9 (fpm)


ЗАПУСК ПОД VAGRANT-ом
---------------------

### Необходимо установить на хост-машине

    * VirtualBox >= 4.3.10 and Extension Pack
    * Vagrant >= 1.7.2
    * Account on https://atlas.hashicorp.com (без аккаунта у вас не зальется Box)
    * Прописать в hosts '171.15.38.10	codepr-test.local'


### Быстрый старт

    git clone git@bitbucket.org:egor_kovyazin/codepr-test-job.git app && cd app \
    && git submodule sync && git submodule init && git submodule update \
    && cd vagrant \
    && vagrant plugin install vagrant-omnibus && vagrant plugin install vagrant-vbguest && vagrant up
    
    echo -e '\n# Test project for codepr.ru\n171.15.38.10 codepr-test.local' >> /etc/hosts


Когда Vagrant успешно подымется (завершится vagrant up), проект будет доступен в браузере по адресу http://codepr-test.local


О ФРЕЙМОРКЕ И ПРИЛОЖЕНИИ
------------------------

Фремйворк Fw - это самописная штука, а точнее: сильно упрощенный Yii v1 (написан специально для тестового задания).


### Что есть

* маршрутизация запросов
* консольные команды, например:

```
#!bash
# Пример команды
php fwc example --verbose
```


### Выполнение тестов

```
#!bash
# Модульные
phpunit --bootstrap tests/unit/_bootstrap.php tests/unit/FileManagerTest.php
```

```
#!bash
# Функциональные (на python)
python tests/functional/python/FileManagerTest.php
```

```
#!bash
# Функциональные (на PHP)
phpunit tests/functional/php/FileManagerTest.php
```

САМО ТЕСТОВОЕ ЗАДАНИЕ
---------------------

1. Создать класс на PHP для поиска и отображения файлов в локальной директории.
1. Покрыть код тестами PHPUnit.
1. Продемонстрировать пример использования класса.
1. Покрыть использование тестами Selenium.