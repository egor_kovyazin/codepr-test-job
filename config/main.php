<?php
/**
 * Основной конфигурационный файл приложения.
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @uses web/index.php | fwc
 */

$rootDir = isset($rootDir) ? $rootDir: __DIR__ . '/..';

return [
	// Путь до корня проекта.
	'basePath' => $rootDir,

	'components' => [

		/**
		 * Настройки подключения через PDO
		 * @uses \core\Application::__construct()
		 */
//		'db' => [
//			'connectionString' => 'mysql:host=127.0.0.1;dbname=_DB-ANME_;charset=utf8',
//			'username'         => '_USER-NAME',
//			'password'         => '_USER-PASSWORD_',
//			'charset'          => 'utf8',
//		],

	],

	'params' => [
		'fileManager' => [
			'dir' => $rootDir . '/components',
		],
	],
];

