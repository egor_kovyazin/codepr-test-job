# -*- coding: utf-8 -*-

#
# Официального webdriver для PHP нет, поэтому хорошо бы писать на питоне
# run: python FileManagerTest.py
# 

import unittest
from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.keys import Keys

class FileManagerTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def tearDown(self):
        self.driver.close()

    # Неуспешный поиск
    def test_search1(self):
        self.driver.get("http://codepr-test.local")
        self.runSearch('t*')
        content = self.getTableContent()
        self.assertEqual(0, len(content))

    # Успешный поиск + переход нв детальную страницу
    def test_search2(self):
        self.driver.get("http://codepr-test.local")
        self.runSearch('File*')
        content = self.getTableContent()
        self.assertNotEqual(0, len(content))

        row = content.pop()
        href = row.find_element_by_xpath("//td//a").get_attribute("href")
        self.driver.get(href)

        assert '"FileManager.php"' in self.driver.page_source
        assert 'namespace components;' in self.driver.page_source



    # Вполнение поискового запроса
    def runSearch(self, query):
        elem = self.driver.find_element_by_name("search[file]")
        elem.clear()
        elem.send_keys(query)
        elem.send_keys(Keys.RETURN)

    # Получение данных из таблицы с результатом
    def getTableContent(self):
        table = self.driver.find_element_by_xpath("//table")
        try:
            return table.find_elements_by_xpath("//tbody//tr")
        except:
            return []



# Run test
unittest.main()
