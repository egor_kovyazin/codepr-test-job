<?php

require_once 'phpwebdriver/WebDriver.php';
require_once 'PHPUnit/Framework/TestCase.php';

/**
 * Функциональные тесты для класса components\FileManager.
 * run: phpunit tests/functional/php/FileManagerTest.php
 * 
 * Для работы используется аhttps://code.google.com/p/php-webdriver-bindings/
 * , так как официальной поддрежки PHP нет.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class FileManagerTest extends PHPUnit_Framework_TestCase
{
	private $selenium;

	public function setUp()
	{
		$this->selenium = new WebDriver('localhost', '4444');
		$this->selenium->connect('firefox', 15);
		$this->selenium->get('http://codepr-test.local');
	}

	public function tearDown()
	{
		$this->selenium->close();
	}

	/**
	 * Поиск с пустым результатом.
	 */
	public function testSearch1()
	{
		$this->search('test*');
		$result = $this->getSearchResult();
		$this->assertEmpty($result);
	}

	/**
	 * Поиск с непустым результатом + переход на страницу содержания файла.
	 */
	public function testSearch2()
	{
		$this->search('*');
		$result = $this->getSearchResult();
		$this->assertNotEmpty($result);

		$href = array_pop($result)->findElementBy(LocatorStrategy::xpath, '//td//a')->getAttribute('href');
		$this->selenium->get($href);

		$title = $this->selenium->findElementBy(LocatorStrategy::xpath, '//h3[@class="panel-title"]')->getText();
		$this->assertEquals('Содежимое файла "FileManager.php"', $title);
	}

	private function getSearchResult()
	{
		try {
			$table = $this->selenium->findElementBy(LocatorStrategy::xpath, '//table');
			return $table->findElementsBy(LocatorStrategy::xpath, '//tbody//tr');
		} catch (Exception $e) {
		}

		return [];
	}

	private function search($query)
	{
		$element = $this->selenium->findElementBy(LocatorStrategy::name, 'search[file]');
		if ($element) {
			$element->sendKeys([$query]);
			$element->submit();
		}
	}
}
