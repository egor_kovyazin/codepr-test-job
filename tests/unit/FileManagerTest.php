<?php

use components\FileManager;

/**
 * Unit тесты для класса components\FileManager.
 * run: phpunit --bootstrap _bootstrap.php FileManagerTest.php
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class FileManagerTest extends PHPUnit_Framework_TestCase
{
	private $filesManager;

	protected function setUp()
	{
		$this->filesManager = FileManager::getInstance();
	}

	protected function tearDown()
	{
		unset($this->filesManager);
	}

	public function testSearch()
	{
		$testCases = [
			'file' => [
				'count' => 0,
				'files' => []
			],
			'f*' => [
				'count' => 2,
				'files' => [
					'file_text.php',
					'file_text.txt'
				]
			],
			'*file*' => [
				'count' => 4,
				'files' => [
					'file_text.php',
					'file_text.txt',
					'text_file_1.txt',
					'text_file_2.txt'
				]
			],
			'folder*' => [
				'count' => 0,
				'files' => []
			],
		];

		foreach ($testCases as $pattern => $expected) {
			$files = $this->filesManager->search($pattern);

			$this->assertEquals($expected['count'], count($files), 'Count founded files for pattern "' . $pattern . '"');

			$filesNames = [];
			if (!empty($files)) {
				$filesNames = array_column($files, 'name');
				sort($filesNames);
			}

			$this->assertArraySubset($filesNames, $expected['files']);
		}
	}

	public function testGetContent()
	{
		$testCases = [
			'file' => null,
			'file_text.txt' => [],
			'exec.php' => [
				"<?php\n",
				"\n",
				'echo __FILE__;'
			],
		];

		foreach ($testCases as $file => $expected) {
			$content = $this->filesManager->getContent($file);

			if (!is_array($expected)) {
				$this->assertEquals($content, $expected, 'Test for file "' . $file . '"');
			} else {
				$this->assertArraySubset($expected, $content, false, 'Test for file "' . $file . '"');
			}
		}
	}

	public function testCheckFileName()
	{
		$testCases = [
			'file'                            => false,
			'file_text.txt'                   => true,
			'folder_1/file_1-to-folder_1.txt' => false,
		];

		$method = $this->getAccessibleMethod('checkFileName');

		foreach ($testCases as $file => $expected) {
			$result = $method->invokeArgs($this->filesManager, [$file]);

			$this->assertEquals($expected, $result, 'Test for file "' . $file . '"');
		}
	}

	public function testCheckForbiddenCharacters()
	{
		$testCases = [
			'../file'                         => true,
			'file_text.txt'                   => false,
			'folder_1/file_1-to-folder_1.txt' => true,
		];

		$method = $this->getAccessibleMethod('checkForbiddenCharacters');

		foreach ($testCases as $pattern => $expected) {
			$result = $method->invokeArgs($this->filesManager, [$pattern]);

			$this->assertEquals($expected, $result, 'Test for file "' . $pattern . '"');
		}
	}

	/**
	 * Делаем доступные скрытые метода.
	 * Чтобы протестировать их.
	 * @todo вообще такое делать нехорошо, но в тестовом задании надо показать, что не лкыком шит :)
	 * 
	 * @param string $methosName
	 * @return ReflectionMethod
	 */
	protected function getAccessibleMethod($methosName)
	{
		$class = new ReflectionClass('\components\FileManager');
		$method = $class->getMethod($methosName);
		$method->setAccessible(true);

		return $method;
	}
}
