<?php

$rootDir = __DIR__ . '/../..';

require_once($rootDir . '/core/autoload.php');

// Загрузка ядря
require_once($rootDir . '/core/Fw.php');

// Загрузка конфига
$config = require_once($rootDir . '/tests/config/main.php');

// Инициализация web-приложения
$app = \core\Fw::createWebApplication($config);

