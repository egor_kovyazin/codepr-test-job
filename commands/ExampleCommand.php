<?php

namespace commands;

use core\Command;

/**
 * Example console command.
 */
class ExampleCommand extends Command
{

	/**
	 * Example default action.
	 */
	public function actionIndex()
	{
		$this->log("Il`s example console command");
	}
}
