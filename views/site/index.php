<?php
/**
 * @var controllers\SiteController $this
 * @var array $files
 * @var array $search
 */

$this->setPageTitle('список/поиск файлов');
?>

<?= $this->renderPartial('_search', ['search' => $search]) ?>

<table class="table table-striped">
	<thead>
		<tr>
			<th>File</th>
			<th>Size (bytes)</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($files as $file): ?>
		<tr>
			<td><?= $file['name'] ?></td>
			<td><?= $file['size'] ?></td>
			<td><a href="/site/view?file=<?= $file['name'] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
