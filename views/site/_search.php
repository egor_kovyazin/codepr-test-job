<?php
/** @var array $search */
?>

<nav class="navbar navbar-default">
	<form class="navbar-form navbar-left" role="search" method="get" action="/">
		<div class="form-group">
			<input type="text" class="form-control" name="search[file]" placeholder="Search file" value="<?= !empty($search['file']) ? htmlspecialchars($search['file']): '' ?>">
		</div>
		<button type="submit" class="btn btn-default">Search</button>
	</form>
</nav>
