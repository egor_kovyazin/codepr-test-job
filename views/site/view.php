<?php
/**
 * @var controllers\SiteController $this
 * @var array $fileRows
 * @var string $filaName
 */

$this->setPageTitle('детализация файла');
?>

<?php if (is_null($fileRows)): ?>

<div class="panel panel-danger">
	<div class="panel-heading">
		<h3 class="panel-title">Ошибка отображения файла "<?= $filaName ?>"</h3>
	</div>
	<div class="panel-body">
		<p>Файла не существует!</p>
	</div>
</div>

<?php else: ?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Содежимое файла "<?= $filaName ?>"</h3>
	</div>
	<div class="panel-body">
		<ol>
			<?php foreach($fileRows as $row): ?>
			<li><pre><code class="html"><?= htmlspecialchars($row) ?></code></pre></li>
			<?php endforeach; ?>
		</ol>
	</div>
</div>

<?php endif; ?>

