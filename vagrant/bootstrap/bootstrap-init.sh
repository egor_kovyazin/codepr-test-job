#!/usr/bin/env bash


##### Configure #####

# Create directories
if [ ! -d /home/vagrant/tmp ]; then
  mkdir /home/vagrant/tmp
  chown vagrant:vagrant /home/vagrant/tmp
fi
if [ ! -d /home/vagrant/logs ]; then
  mkdir /home/vagrant/logs
  chown vagrant:vagrant /home/vagrant/logs
fi

# Replace nginx configuration file
if [ -f /etc/nginx/sites-enabled/default ]; then
  rm /etc/nginx/sites-enabled/default
fi
ln -sfn /vagrant/bootstrap/config/nginx.conf /etc/nginx/sites-enabled/app.conf
service nginx restart

# Replace php-fpm configuration file
if [ -f /etc/php5/fpm/pool.d/www.conf ]; then
  rm /etc/php5/fpm/pool.d/www.conf
fi
ln -sfn /vagrant/bootstrap/config/fpm.conf /etc/php5/fpm/pool.d/www.conf
service php5-fpm restart
