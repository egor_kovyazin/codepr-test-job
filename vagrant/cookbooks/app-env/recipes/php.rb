package "php5-common"
package "php5-fpm"
package "php5-cli"
package "php5-cgi"

package "php-pear"
package "make"
package "php5-curl"
package "php5-mysql"
package "php5-dev"


bash "set-timezone" do
  user "root"
  code <<-EOH
    sed -i 's/;date.timezone.*/date.timezone = "UTC"/g' /etc/php5/fpm/php.ini
    sed -i 's/;date.timezone.*/date.timezone = "UTC"/g' /etc/php5/cli/php.ini
    service php5-fpm restart
  EOH
end


bash "install-php-unit" do
  user "root"
  returns [0, 1]
  cwd "/tmp"
  code <<-EOH
    wget https://phar.phpunit.de/phpunit-old.phar --output-document=phpunit.phar
    chmod +x phpunit.phar
    mv phpunit.phar /usr/local/bin/phpunit
  EOH
end
