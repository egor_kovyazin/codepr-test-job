package "nginx" do
  action :install
  options "--force-yes"
end

# Enable nginx service on system startup
service "nginx" do
  supports :status => true, :restart => true, :reload => true
  action :enable
end