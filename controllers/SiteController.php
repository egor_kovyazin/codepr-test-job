<?php

namespace controllers;

use core\Controller;
use components\FileManager;

/**
 * Контроллер "site"
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class SiteController extends Controller
{
	/**
	 * Страница поиска и отображения списка файлов.
	 */
	public function actionIndex()
	{
		$search['file'] = !empty($_GET['search']['file'])
			? $_GET['search']['file']
			: null;

		$this->render('index', [
			'files'  => FileManager::getInstance()->search($search['file']),
			'search' => $search,
		]);
	}

	/**
	 * Отображение содержимого файла.
	 */
	public function actionView()
	{
		if (!isset($_GET['file'])) {
			header("HTTP/1.0 404 Not Found");
		}

		$filaName = $_GET['file'];

		$this->render('view', [
			'fileRows' => FileManager::getInstance()->getContent($filaName),
			'filaName' => $filaName,
		]);
	}

}
